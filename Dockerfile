FROM thefoundation/hocker:php8.1-dropbear-fpm

MAINTAINER thefoundation@gitlab.io


##speed up testing by uncommenting
###RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
#RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash && bash -c "cd /root;source .bashrc;export NVM_DIR=/root/.nvm ;. /root/.nvm/nvm.sh;nvm list-remote;echo nvm install v12.16.3 ;nvm install lts/gallium ;node -v"
#RUN apt update && apt -y install npm && npm install --global gulp-cli 
#RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
#RUN  apt-get install -y nodejs
#RUN  npm install --global gulp-cli
#RUN test -e /var/www/html && rm -rf /var/www/html || true
#RUN apt update && apt -y install npm && 
#RUN su -s /bin/bash  -c "git clone https://github.com/afterlogic/webmail-lite-8.git /tmp/afterlogic ; " www-data 
##RUN su -s /bin/bash  -c "cd /tmp/afterlogic; npm install --global gulp-cli  &  composer install & wait ; npm install;npm audit fix --force;" www-data|| true
##RUN su -s /bin/bash  -c "cd /tmp/afterlogic; cd modules/AdminPanelWebclient/vue ; npm install;npm install -g @quasar/cli;npm audit fix;" www-data
##RUN su -s /bin/bash  -c "cd /tmp/afterlogic; gulp styles --themes Default,DeepForest,Funny,Sand;gulp js:min;" www-data 
##RUN grep -rl -e 4096 -e 2048 -e 2047 /tmp/afterlogic|grep -e pgp -e app 
##RUN su -s /bin/bash  -c "cd /tmp/afterlogic; cd modules/AdminPanelWebclient/vue;npm run build-production;  " www-data 
##RUN mv /tmp/afterlogic /var/www/html && chown -R www-data:www-data /var/www/html/
#&& apt remove npm && apt-get autoremove && apt-get clean && mv /tmp/afterlogic /var/www/html


RUN  ( test -e /var/www/html && rm -rf /var/www/html || true ) && su -s /bin/bash  -c "cd /var/www/;mkdir html;cd html;wget -c https://afterlogic.org/download/webmail_php.zip && unzip webmail_php.zip && rm webmail_php.zip " www-data 2>&1 |grep -v -e inflating -e extracting -e creating
RUN /bin/bash -c "chown -R www-data:www-data /var/www/html/ & chmod a-w -R /var/www/html & wait; chmod ug+rw -R /var/www/html/data"


VOLUME /var/www/html/data
RUN rm -rf /usr/local/lib/node_modules /var/www/.npm  || true 
RUN ls -lh1 /var/www/html/index.php
